//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//soal 2
var kataPertama = "wah";
var kataKedua = "java script";
var kataKetiga = "itu";
var kataKeempat = "keren";
var kataKelima = "sekali";

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga =  kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

